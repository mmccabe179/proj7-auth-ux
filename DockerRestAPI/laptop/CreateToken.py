from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
from flask import Flask

import time

app = Flask(__name__)
app.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy dog'

def generate_auth_token(user_id, expiration=600):
	s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
	# pass index of user
	return s.dumps(user_id)

def verify_auth_token(token):
	s = Serializer(app.config['SECRET_KEY'])
	try:
		data = s.loads(token)
	except SignatureExpired:
		return False		# valid token, but expired
		app.logger.debug("signature expired")
	except BadSignature:
		return False		# invalid token
		app.logger.debug ("bad signature")
	return True

