#! /usr/bin/env python
import json
import csv
import flask
from flask import Flask, request, Response, render_template, abort, jsonify, flash, redirect, url_for
from flask_restful import Resource, Api, reqparse
from pymongo import MongoClient
import os
import pymongo
from config import Config

from CreateToken import generate_auth_token, verify_auth_token
from password import hash_password, verify_password

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired

from flask_login import login_user, logout_user, LoginManager, UserMixin, login_required




# Instantiate the app
app = Flask(__name__)
api = Api(app)

login_manager = LoginManager()
login_manager.init_app(app)

client = MongoClient('172.18.0.2', 27017)
db = client.tododb
db_ = client.login

#db_.login.delete_many({})

app.config['SECRET_KEY'] = "the quick brown fox jumps over the lazy dog"

class User(UserMixin):
    def __init__(self, user_id):
        self.id = str(user_id)

@login_manager.user_loader
def load_user(username):
    user = db_.login.find_one({'username': username})
    if user == None:
    	return None
    app.logger.debug(User(user['_id']))
    return User(user['_id'])

@app.route('/register', methods=['POST'])
def getUser(password, username):

	hashed = hash_password(password) #create a hashed_password (unicode) from password.py (provided)
	#app.logger.debug(password)
	#app.logger.debug(username)

	if username == None or password == None: #if either the username or password are None values abort(400)
		abort(400)					 	 #standardized abort messages 

	username_ = { 'username': username, 'password': hashed }

	db_.login.insert_one(username_)

	#need to make _id into a string to use in the dictionary
	#_id is standard mongodb to access a unique id for db entry
	#_id has to be string format to be json serializable

	user = db_.login.find_one({'username' : username})

	#token = generate_auth_token(str(user['_id']))

	result = {'username': user['username'], 'password': user['password'], '_id': str(user['_id'])}

	return flask.jsonify(result=result)	


def CreateToken(_id):
	#first check if the user is in the db 
	#function returns a token

	token = generate_auth_token(_id) #tokenize the id using generate_auth_token and id


	result = {'token': str(token), 'expiration': 600}

	app.logger.debug(result)

	return result


class LoginForm(FlaskForm):
	username = StringField('Username', validators=[DataRequired()])
	password = PasswordField('Password', validators=[DataRequired()])
	remember_me = BooleanField('Remember Me')
	submit = SubmitField('Sign In')


class RegisterForm(FlaskForm):
	username = StringField('Username', validators=[DataRequired()])
	password = StringField('Password', validators=[DataRequired()])
	remember_me = BooleanField('Remember Me')
	submit = SubmitField('Register')

@app.route('/api/token', methods=['GET', 'POST'])
def login():
	form = LoginForm()
	app.logger.debug(form.username.data)
	app.logger.debug(form.password.data)


	if form.validate_on_submit():
		user = db_.login.find_one({'username': form.username.data})
		app.logger.debug(user)
		app.logger.debug(form.password.data)
		if user == None:
			abort(401)

		app.logger.debug(verify_password(form.password.data, user['password']))
		if user != None and verify_password(form.password.data, user['password']):

			log_in = User((user['_id']))

			login_user(log_in)
			app.logger.debug(login_user(log_in))

			_id = str(user['_id'])

			createToken = CreateToken(_id)
			token = createToken['token']

			flash('Login requested for user {}, remember_me={}, token={}'.format(form.username.data, form.remember_me.data, token))
		else:
			abort(401)

	return render_template('login.html',  title='Sign In', form=form)

@app.route('/api/register', methods=['GET', 'POST'])
def register():
	form = RegisterForm()
#	app.logger.debug(form.username.data)
#	app.logger.debug(form.password.data)

	if form.validate_on_submit():
		user = db_.login.find_one({'username': form.username.data})
		if user != None: 	#user is already registered 
			abort(401)
		getUser(form.password.data, form.username.data) #getUser function to produce db for registered user
	return render_template('register.html', form=form)


@app.route("/api/logout")
def logout():
    logout_user()
    flash("Logged out.")
    return redirect(url_for("login"))

def getToken():

	token = request.args.get('token')
	app.logger.debug(token)
	#string_id = str(_id)

	#new_id = (string_id + "M")
	#app.logger.debug(str(new_id))

	#dbtoken = _db_.token.find_one({"index": str(_id + 'M')})

	#token = dbtoken['token']

	#app.logger.debug(token)

	if str(token) == None:
		abort(401)

	verify = verify_auth_token(str(token))

	app.logger.debug(verify)

	return verify

class listAll(Resource):
	def get(self):
		if getToken() == False:
			abort(401)
		else: 
			top = request.args.get('top', 0, type=int)
			if top is not 0:
				result = getTimes(top, True, True)
			else:
				result = getTimes(None, True, True)
				return flask.jsonify(result=result)

class listOpenOnly(Resource):
	def get(self):
		if getToken() == False:
			abort(401)
		else:
			top = request.args.get('top', 0, type=int)
			if top is not 0:
				result = getTimes(top, True, False)
			else:
				result = getTimes(None, True, False)
			return flask.jsonify(result=result)

class listCloseOnly(Resource):
	def get(self):
		if getToken() == False:
			abort(401)
		else:
			top = request.args.get('top', 0, type=int)
			if top is not 0:
				result = getTimes(top, False, True)
			else:
				result = getTimes(None, False, True)
			return flask.jsonify(result=result)

class listAllCSV(Resource):
	def get(self):
		if getToken() == False:
			abort(401)
		else:
			top = request.args.get('top', 0, type=int)
			if top == None:
				top = 20
			_items = db.tododb.find().limit(int(top))
			items = [item for item in _items]
			csv = ""
			for item in items:
				csv += item['open'] + ', ' + item['close'] + ', '
			return csv

class listOpenOnlyCSV(Resource):
	def get(self):
		if getToken() == False:
			abort(401)
		else:
			top = request.args.get('top', 0, type=int)
			if top == None:
				top = 20
			_items = db.tododb.find().limit(int(top))
			items = [item for item in _items]
			csv = ""
			for item in items:
				csv += item['open'] + ', '
			return csv

class listCloseOnlyCSV(Resource):
	def get(self):
		if getToken() == False:
			abort(401)
		else:
			top = request.args.get('top', 0, type=int)
			if top == None:
				top = 20
			_items = db.tododb.find().limit(int(top))
			items = [item for item in _items]
			csv = ""
			for item in items:
				csv += item['close'] + ', '
			return csv


def getTimes(top,Open,Close):
	limit = 20

	if top is not None:
		limit = top

	times = db.tododb.find().limit(int(limit))
	result = []
	for item in times:
		if Open and Close:
			result.append({
				'open': item['open'],
				'close': item['close'],
				'km': item['km']
				})
		elif Open:
			result.append({
				'open': item['open'],
				'km': item['km']
				})
		else:
			result.append({
				'close': item['close'],
				'km': item['km']
				})
	return result


	
# Create routes
# Another way, without decorators
api.add_resource(listAll, '/listAll', '/listAll/json')
api.add_resource(listOpenOnly, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(listCloseOnly, '/listCloseOnly', '/listCloseOnly/json')
api.add_resource(listAllCSV, '/listAll/csv')
api.add_resource(listOpenOnlyCSV, '/listOpenOnly/csv')
api.add_resource(listCloseOnlyCSV, '/listCloseOnly/csv')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
