#! /usr/bin/env python
import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient

import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations

import config

import logging

app = Flask(__name__)

client = MongoClient('172.18.0.2', 27017)


db = client.tododb


app.secret_key = 'super secret key'

submit = []



@app.route('/new', methods=['POST'])
def new():
    #open_times = []                         #Create an empty array to hold open times
    #close_times = []                        #Create an empty array to hold close times
    #for item in open_times:
    db.tododb.delete_many({})
    for dictionary in submit:
        item_doc = {
            'brevet_distance': dictionary["distance"],
            'km': dictionary["km"],
            'start': dictionary['start time'],
            'opdate': dictionary['open date'],
            'cldate': dictionary['close date'],
            'open': dictionary["open time"],
            'close': dictionary["close time"]
            }
            

        db.tododb.insert_one(item_doc)
    count = 0
    for dictionary in submit:
        count = count + 1

    app.logger.debug(count)

    if submit == []:
        return redirect(url_for('no_entry'))
        
    for dictionary in submit:
        if dictionary['km'] > dictionary['distance']:
            del submit[:]
            return redirect(url_for('too_large'))

  
    last = submit[-1:]
    for item in last:
        if item["km"] != item["distance"]:
            return redirect(url_for('ending'))
            del submit[:]


    del submit[:]

    return redirect(url_for('index'))


@app.route("/")
@app.route("/index")
def index():
    del submit[:]
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

@app.route("/no_entry")
def no_entry():
    app.logger.debug("No Input Items")
    return render_template('no_entry.html')

@app.route("/too_large")
def too_large():
    app.logger.debug("Input is larger than brevet distance")
    return render_template('too_large.html')

@app.route("/ending")
def ending():
    app.logger.debug("last brevet km does not equal the brevet distance")
    return flask.render_template('not_enough.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404



@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('brevet_distance_km' , 999, type=int)
    stime = request.args.get('start_time', 999, type=str)
    sdate = request.args.get('start_date', 999, type=str)

    #app.logger.debug("start time {}".format(stime))
    #app.logger.debug("start date {}".format(sdate))
    #app.logger.debug("km={}".format(km))
    #app.logger.debug("request.args: {}".format(request.args))
    
    time_str = "{}T{}".format(sdate, stime)
    time = arrow.get(time_str)
    time = time.isoformat() 
    
    open_time = acp_times.open_time(km, distance, time)
    close_time = acp_times.close_time(km, distance, time)

    result = {"open": open_time, "close": close_time}

    start_date, open_time = open_time.split("T")
    close_date, close_time = close_time.split("T")

    dictionary = {"km" : km, "distance" : distance, "open time" : open_time, "close time" : close_time, "start time": stime, "open date": start_date, "close date": close_date}

    submit.append(dictionary)
    #app.logger.debug(submit)

    return flask.jsonify(result=result)


@app.route("/display", methods=['POST'])
def display():
    app.logger.debug("some display message")
    _items = db.tododb.find()
    items = [item for item in _items]
    if items == []:
        return redirect(url_for('no_entry'))
    else:
        return flask.render_template('open_close_times.html', items=items)


#############

if __name__ == "__main__":
    app.run(host='0.0.0.0', port = 5000, debug=True)
