# Project 7: Adding authentication and user interface to brevet time calculator service

To Run:
	go to /DockerRestAPI and run docker-compose up
	localhost:5001 hosts api resources

	localhost:5001/api/register requires registration of username and password
		After registering a username and password those credentials can be used on the log in page (localhost:5001/api/token)

	localhost:5001/api/token login using registered username and password
		After logging in a registered username and password a message will display 
		Login requested for user newuser7, remember_me=False, token=b'<token>'

		This token should be copied NOT INCLUDING the b' at the beginning. 
		This token should be used to access the rest of the api resources

	localhost:5001/api/logout will logout the current user using flask-login

	localhost:5005 hosts the acp brevet times calculator. Here the user can input controle times to see outputted open and close times. These times are entered into a mongodb database and can be accessed through the following API's INCLUDING the authroized token generated above. 

	localhost:5001/listAll?token=''
	localhost:5001/listOpenOnly?token=''
	localhost:5001/listCloseOnly?token=''

	localhost:5001/listAll/csv?token=''
	localhost:5001/listAll/json?token=''

	localhost:5001/listOpenOnly/csv?token=''
	localhost:5001/listOpenOnly/json?token=''

	localhost:5001/listCloseOnly/csv?token=''
	localhost:5001/listCloseOnly/json?token=''

	A 'top' parameter can also be added to any of these API resources to see the top 'n' amount of results in the database. 